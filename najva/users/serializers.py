from rest_framework import serializers

from .models import User


class UserAuthenticationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'password', 'age', 'gender']

    def create(self, validated_data):
        new_user = User.objects.create_user(**validated_data)
        return new_user


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username']