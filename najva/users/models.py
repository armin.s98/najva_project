from django.db import models
from django.contrib.auth.models import AbstractUser, update_last_login


class User(AbstractUser):
    pass


class Karjoo(User):
    def login_handle(self):
        last_login = self.last_login
        update_last_login(None, self)
        new_login = self.last_login
        self.add_daily_credit(last_login, new_login)


class Company(User):
    pass