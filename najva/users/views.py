from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.exceptions import InvalidToken
from rest_framework.views import APIView
from rest_framework import permissions

from .models import User
from .permissions import IsNotAuthenticated
from .serializers import UserAuthenticationSerializer, UserProfileSerializer


class UserLogin(TokenObtainPairView):
    def post(self, request, *args, **kwargs):
        try:
            response = super(UserLogin, self).post(request, *args, **kwargs)
            if response.status_code == HTTP_200_OK:
                user = User.objects.get(username=request.data["username"])
                user.login_handle()
            return response
        except InvalidToken:
            return Response(status=HTTP_400_BAD_REQUEST)


class UserSignupView(APIView):
    permission_classes = [IsNotAuthenticated]

    def post(self, request, *args, **kwargs):
        serializer = UserAuthenticationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class UserLogout(APIView):
    permissions_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        try:
            request.user.auth_token.delete()
        except (AttributeError, ObjectDoesNotExist):
            pass

        return Response(status=HTTP_200_OK)


class ViewProfiles(APIView):
    permissions_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        username = request.query_params.get('username')
        if username is None:
            return Response(status=HTTP_400_BAD_REQUEST)
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return Response(status=HTTP_404_NOT_FOUND)
        serializer = UserProfileSerializer(user)
        data = serializer.data
        if username == request.user.username:
            pass
            #TODO

        return Response(data, status=HTTP_200_OK)